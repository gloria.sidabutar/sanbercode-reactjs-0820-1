//soal 1

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var textgabung = (kataPertama.concat(" ",(kataKedua.toUpperCase().charAt(0))+kataKedua.substring(1)," ",kataKetiga," ",kataKeempat.toUpperCase()));

console.log(textgabung);

//soal 2

var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

hasil1= parseInt(kataPertama); 
hasil2= parseInt(kataKedua); 
hasil3= parseInt(kataKetiga); 
hasil4= parseInt(kataKeempat);

var hitung = hasil1 + hasil2 + hasil3 + hasil4;
console.log (hasil1,"+",hasil2, "+", hasil3,"+",hasil4, "=", hitung )

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.slice(15, 18);
var kataKeempat = kalimat.slice(18, 24);
var kataKelima = kalimat.slice(24, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keemapat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);  

//soal 4
var nilai;
var nilai = 75;
if ( nilai >= "80" ) {
    console.log("indeksnya A")
} else if ( nilai >= "70" ) {
    console.log("indeksnya B")
} else if ( nilai >= "60" ) {
    console.log("indeksnya C") 
} else if ( nilai >= "50" )  {
    console.log("indeksnya D") 
} else {
    console.log("indeksnya E") 
}

//soal 5
var tanggal = 17;
var bulan = 3;
var tahun = 1999;

switch(bulan) {
    case 1:   { console.log(tanggal + ' Januari ' + tahun); break; }
    case 2:   { console.log(tanggal + ' Februari ' + tahun); break; }
    case 3:   { console.log(tanggal + ' Maret ' + tahun); break; }
    case 4:   { console.log(tanggal + ' April ' + tahun); break; }
    case 5:   { console.log(tanggal + ' Mei ' + tahun); break; }
    case 6:   { console.log(tanggal + ' Juni ' + tahun); break; }
    case 7:   { console.log(tanggal + ' Juli' + tahun); break; }
    case 8:   { console.log(tanggal + ' Agustus ' + tahun); break; }
    case 9:   { console.log(tanggal + ' September ' + tahun); break; }
    case 10:   { console.log(tanggal + ' Oktober ' + tahun); break; }
    case 11:   { console.log(tanggal + ' November ' + tahun); break; }
    case 12:   { console.log(tanggal + ' Desember ' + tahun); break; }}