
  //soal 1
  console.log("--soal 1--")
  const pi = 3.14
  let luasLingkaran = (jariJari) => pi * jariJari *jariJari ;
  let kelilingLingkaran = (jariJari) => pi * jariJari * 2
  
  console.log(luasLingkaran(3));
  console.log(kelilingLingkaran(3))


//soal 2
console.log("--soal 2--")
let kata1 = 'saya'
let kata2 = 'adalah'
let kata3 = 'seorang'
let kata4 = 'frontend'
let kata5 = 'developer'
let kalimat = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`
console.log(kalimat)


//soal 3 
console.log("--soal 3--")
const newFunction = function literal(firstName, lastName){
return {
  firstName, lastName, fullName(){
    console.log(`${firstName} ${lastName}`)
    return 
  }
}
}
//Driver Code 
newFunction("William", "Imoh").fullName() 

// soal 4
console.log("--soal 4--")
const newObject = {
firstName: "Harry",
lastName: "Potter Holt",
destination: "Hogwarts React Conf",
occupation: "Deve-wizard Avocado",
spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject

console.log(firstName, lastName, destination, occupation)

//soal 5
console.log("--soal 5--")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
//Driver Code
console.log(combined)